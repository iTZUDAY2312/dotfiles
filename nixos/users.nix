# user specific configuration

{ config, pkgs, inputs, lib, ... }:

{

  # User account
  users.users.uday = {
    shell = pkgs.fish;
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "adbusers" ]; # Enable ‘sudo’ for the user.
  #   packages = with pkgs; [
  #     firefox
  #     tree
  #   ];
  };
  
}
