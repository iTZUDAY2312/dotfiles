# Desktop/Window Manager stuff

{ config, pkgs, inputs, lib, ... }: 

{

  # XDG Portal
  xdg.portal = {
    enable = true;
    extraPortals = with pkgs; [
      xdg-desktop-portal-wlr
      xdg-desktop-portal-hyprland
    ];
  };
  
  # X Server  
  # services.xserver = {

    # Enable the X11 windowing system.
    # enable = true;

    # Desktop Environment
    # desktopManager = {
      # xfce.enable = true;
      # lxqt.enable = true;
      # gnome.enable = true;
      # plasma5.enable = true;
    # };

    # Display Manager
    # displayManager = {
      # gdm.enable = true;
      # sddm.enable = true;
      # lightdm = {
           # enable = true;
           # greeter.enable = true;
      # };
    # };
   
  # };
  
  # Touchpad & Mouse
  services.xserver.libinput = {
    enable = true; # Enable touchpad support
    # mouse.accelProfile = "flat"; # Disable mouse acceleration
    # mouse.accelSpeed = "0.5";
    # touchpad.accelSpeed = "0.4";
    # mouse.middleEmulation = false;

    };
  
  # Graphics
  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
    extraPackages = with pkgs; [
      vaapiVdpau
      libva
      libvdpau-va-gl
      mesa.drivers
      libvpx
    ];
  };

}
