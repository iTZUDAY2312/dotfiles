# NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, inputs, lib, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ./base.nix
      ./users.nix
      ./gui.nix
      ./packages.nix
    ];

  # System
  # system.copySystemConfiguration = true;
  system.stateVersion = "23.05";

}
