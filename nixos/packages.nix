# Package configuration file

{ config, pkgs, inputs, lib, ... }:

{ 
 
  # Programs with nix options
  programs = {
    hyprland.enable = true;
    waybar.enable = true;
    nm-applet.enable = true;
    adb.enable = true;
    fish.enable = true;
  };
  
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  
  environment.systemPackages = with pkgs; [
      
    # Basic packages & tools
    ntfs3g
    wget
    aria
    axel
    hwinfo
    neofetch
    cpufetch
    pfetch
    htop
    btop
    cmatrix
    cava

    # Personal apps
    alacritty
    xarchiver
    firefox-wayland
    tdesktop 
    obs-studio
    mpv
    vscodium
    gparted
    onlyoffice-bin

    # Other Stuff
    vim
    neovim
    git
    gh
    glab
    openssh
    tmux
    tmate
    brightnessctl
    pavucontrol
    pamixer
    unzip
    figlet
    android-tools
    lm_sensors
    libnotify
    libva-utils
    libsixel
    libfaketime

    # XFCE stuff
    xfce.thunar
    xfce.thunar-volman
    xfce.thunar-archive-plugin
    xfce.thunar-media-tags-plugin
    xfce.tumbler
    xfce.ristretto
    xfce.xfce4-power-manager

    # Xorg stuff
    xorg.xbacklight
    xorg.xdpyinfo
    xorg.xf86videoamdgpu
    xorg.xf86inputlibinput
    xorg.xf86inputvoid
    xorg.xkill
    xorg.xmodmap
    xorg.xhost

    # QT Stuff
    libsForQt5.qt5.qtwayland
    libsForQt5.qt5.qtquickcontrols
    libsForQt5.qt5.qtquickcontrols2
    libsForQt5.qt5.qtgraphicaleffects
    qt6.qtwayland
    qt6.qtimageformats

    # Hyprland and related pkgs
    networkmanagerapplet
    wlr-randr
    polkit
    libsForQt5.polkit-qt
    libsForQt5.polkit-kde-agent
    cinnamon.nemo-with-extensions
    hyprpaper
    dunst
    wofi
    rofi-wayland
    kitty
    wlogout
    swww
    swaylock-effects
    swappy
    grim
    slurp
    cliphist
    wl-clipboard
 ];

  fonts.fonts = with pkgs; [
    font-awesome
    nerdfonts
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    fira-code
    fira-code-symbols
  ];

}
