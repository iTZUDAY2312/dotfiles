# Base system configuration

{ config, pkgs, inputs, lib, ... }:

{

  # Kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # Microde
  hardware.cpu.amd.updateMicrocode = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
    efi.efiSysMountPoint = "/boot";
  };
  
  # Set your time zone.
  time.timeZone = "Asia/Kolkata";
  services.ntp.enable = true;
  services.timesyncd.enable = true;
  
  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
    # useXkbConfig = true; # use xkbOptions in tty.
  };
  
  # Networking Options
   
  networking = {
    hostName = "nixos"; # Your hostname.
    # Pick only one of the below networking options.
    networkmanager.enable = true;  # Easiest to use
    # wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  };
  
  # Configure network proxy if necessary
  # networking.proxy = {
    # default = "http://user:password@proxy:port/";
    # noProxy = "127.0.0.1,localhost,internal.domain";
  #   };

  # Open ports in the firewall.
  # networking.firewall = {
    # allowedTCPPorts = [ ... ];
    # allowedUDPPorts = [ ... ];
    # enable = false; # Or disable the firewall altogether.
  # };
  
  # Configure keymap in X11
  services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e,caps:escape";

  # Enable sound.
  sound.enable = true;
  # hardware.pulseaudio.enable = true;
  services.pipewire = {
    enable = true;
    audio.enable = true;
    pulse.enable = true;
    jack.enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    wireplumber.enable = true;
  };

  # Bluetooth
  hardware.bluetooth = {
    enable = true;
    package = pkgs.bluez;
  };

  services.blueman.enable = true;

  # Enable CUPS to print documents.
  # services.printing.enable = true;
   
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

}
