#!/bin/bash

##################################### START #####################################
#
# Make Partitions with cfdisk & format fs
# Now mount partitions properly
#

echo "
██████╗░██╗░░░██╗  
██╔══██╗╚██╗░██╔╝  
██████╦╝░╚████╔╝░  
██╔══██╗░░╚██╔╝░░  
██████╦╝░░░██║░░░  
╚═════╝░░░░╚═╝░░░  

██╗░░░██╗██████╗░░█████╗░██╗░░░██╗
██║░░░██║██╔══██╗██╔══██╗╚██╗░██╔╝
██║░░░██║██║░░██║███████║░╚████╔╝░
██║░░░██║██║░░██║██╔══██║░░╚██╔╝░░
╚██████╔╝██████╔╝██║░░██║░░░██║░░░
░╚═════╝░╚═════╝░╚═╝░░╚═╝░░░╚═╝░░░   "

# Run in chroot
# arch-chroot /mnt

# Base Packages
echo "Installing Base Packages............."
pacman -S base base-devel linux linux-headers linux-firmware nano networkmanager iwd amd-ucode --needed 

# fstab gen
echo "Generating fstab ....."
genfstab -U /mnt >> /mnt/etc/fstab

# Hosts
echo "setting hostname"
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain archlinux" >> /etc/hosts
echo "arch" >> /etc/hostname

# Locale
echo "Setting Locale"
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen

# Timezone
echo "Setting timezone"
ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime
hwclock --systohc

# Services
echo "enabling system services"
systemctl enable NetworkManager
systemctl enable iwd

# GRUB
echo "Installing & Configuring GRUB Bootloader"
pacman -S grub efibootmgr ntfs-3g os-prober
echo "GRUB_DISABLE_OS_PROBER=false" >> /etc/default/grub
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

# add root passwd & sudo user
echo "Setting root password"
passwd 
echo "Adding User with sudo access"
echo"Enter User name"
read username
useradd -m itzuday2312
passwd itzuday2312
usermod -aG wheel,storage,power,audio itzuday2312
echo "itzuday2312 ALL=(ALL:ALL) ALL" >> /etc/sudoers
# Or Do below steps
# nano /etc/sudoers
# uncomment %wheel ALL=(ALL:ALL) ALL

# Installing yay 
git clone https://aur.archlinux.org/yay-git.git
cd yay-git && makepkg -si
cd ..
rm -rf yay-git

 ##################################### END #####################################
